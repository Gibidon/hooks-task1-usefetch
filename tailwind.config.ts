import type { Config } from 'tailwindcss'

export default {
  content: ['./public/**/*.{html,js,ts}','./src/**/*.{ts,tsx}'],
  theme: {
    extend: {},
  },
  plugins: [],
} satisfies Config
