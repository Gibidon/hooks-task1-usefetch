import { useEffect, useState } from 'react'

type FetchParams = { method?: 'GET' | 'POST'; params?: { _limit?: number } }

export function useFetch<T>(url: string) {
  const [data, setData] = useState<T>(null)
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState(false)

  //how to type params correctly?
  function refetch({ params }: any) {
    const newParams = new URLSearchParams(params)
    loadData<T>(`${url}?${newParams.toString()}`).then((data) => setData(data))
  }

  async function loadData<T>(url: string): Promise<T> {
    try {
      setIsLoading(true)
      const response = await fetch(url)

      if (!response.ok) {
        throw new Error(`Error: ${response.status}`)
      }

      const data = await response.json()
      return data
    } catch (e) {
      console.log(e.message)

      setError(true)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    loadData<T>(url).then(setData)
  }, [])

  return { data, isLoading, error, refetch }
}
