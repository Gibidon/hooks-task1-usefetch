import { useFetch } from './hw_hooks/useFetch'

interface IPost {
  id: number
  userId: number
  title: string
  body: string
}

export const App = () => {
  const { data, isLoading, error, refetch } = useFetch<IPost[]>(
    'https://jsonplaceholder.typicode.com/posts',
  )

  return (
    <>
      <div>
        <button
          onClick={() =>
            refetch({
              params: {
                _limit: 3,
              },
            })
          }
          className="bg-orange-300 p-2 m-1 rounded-md"
        >
          Перезапросить
        </button>
      </div>
      {isLoading && 'Загрузка...'}
      {error && 'Произошла ошибка'}
      {error && error}
      {data &&
        !isLoading &&
        data.map((item) => (
          <div key={item.id} className="border-b-2">
            {item.title}
          </div>
        ))}
    </>
  )
}
